import 'package:fish_redux/fish_redux.dart';

import 'state.dart';

Effect<LeftListState> buildEffect() {
  return combineEffects(<Object, Effect<LeftListState>>{});
}
