import 'package:fish_redux/fish_redux.dart';

import 'state.dart';

Reducer<RightListState> buildReducer() {
  return asReducer(
    <Object, Reducer<RightListState>>{},
  );
}
