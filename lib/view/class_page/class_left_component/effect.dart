import 'package:fish_redux/fish_redux.dart';
import 'action.dart';
import 'state.dart';

Effect<ClassLeftState> buildEffect() {
  return combineEffects(<Object, Effect<ClassLeftState>>{});
}
