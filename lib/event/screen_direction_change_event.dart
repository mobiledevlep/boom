class ScreenDirectionChangeEvent {
  ///是否是竖屏
  final bool isPortraitUp;

  ScreenDirectionChangeEvent({this.isPortraitUp});
}
